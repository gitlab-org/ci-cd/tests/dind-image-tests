# dind-image-tests

Test that GitLab CI works with various DinD images

## Prepare test jobs definitions

To update the test jobs definitions to refference new versions (or remove old ones, that we're
no more interested in testing), do the following:

1. Update the `versions.txt` file by adding and/or removing the versions that you're
   interested in.

1. Execute `./prepare.sh` to regenerate the `.gitlab-ci-versions.yml` file.

1. Commit and push the changes.
